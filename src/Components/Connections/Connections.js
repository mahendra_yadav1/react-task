import React, { Component } from "react";
import Connection from "./connection";
import { connect } from "react-redux";

class connections extends Component {
  state = {
    conn: [],
    searchedConn:[],
    user: "",
    searching:false
  };

  
  componentDidMount() {
    this.props.userConn.map(con => {
      fetch(`http://localhost:3001/${con}`)
        .then(data => data.json())
        .then(data => {
          this.setState(prevState => {
            return {
              conn: [
                ...prevState.conn,
                { username: data.username, status: data.status, avatar: data.avatar }
              ]
            };
          });
        })
        .catch(err => console.log(err));
      return null;
    });
  }

  handleSearch = () => {
    this.setState({searching:true});
    this.state.conn.map((con)=>{
      if(con.username.includes(this.state.user)){
        this.setState(
          (prevState)=>{
        return{searchedConn : [...prevState.searchedConn,{username : con.username, status:con.status, avatar : con.avatar}],searching:true}
        }
          );
      }
      return null;
    });
    
  }

  render() {
    let conn = null;
    let searchedConn = null;
    conn = this.state.conn.map((con, index) => (
      <Connection key={index} conn={con} />
    ));

    searchedConn = this.state.searchedConn.map((con, index) => (
      <Connection key={index} conn={con} />
    ));

    return (
      <div>
        <h4>My Connections</h4>
        <input
          type="text"
          placeholder="Search Connection"
          value={this.state.user}
          onChange={evt => this.setState({ user: evt.target.value })}
        />
        <button onClick={this.handleSearch}>Go</button>
        <br />
        {this.state.searching? searchedConn : conn}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userConn: state.data.userData.connections
  };
};

export default connect(mapStateToProps)(connections);
