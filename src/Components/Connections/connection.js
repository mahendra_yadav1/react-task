import React from "react";
import "./connection.css";

const connection = props => {
  return (
    <div className="card">
      <img
        src={props.conn.avatar}
        alt="Avatar"
        width="100%"
        height="auto"
      />
      <b>{props.conn.username}</b>
      <br />
      <span>{props.conn.status}</span>
    </div>
  );
};

export default connection;
