import React from "react";
import { connect } from "react-redux";

const header = props => {
  return (
    <div className="head">
      <h3>{props.username}</h3>
      <button className="logout" onClick={props.handler}>
        logout
      </button>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    username: state.data.userData.username
  };
};

export default connect(mapStateToProps)(header);
