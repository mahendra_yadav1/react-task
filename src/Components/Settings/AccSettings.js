import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

const accSettings = props => {
  return (
    <div className="left">
      <Link to="/mahendra/myposts">My Posts</Link>
      <br />
      <Link to={`/${props.username}/edit`}>Account Settings</Link>
    </div>
  );
};
const mapStateToProps = state => {
  return {
    username: state.data.userData.username
  };
};

export default connect(mapStateToProps)(accSettings);
