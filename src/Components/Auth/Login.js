import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";

import "./Login.css";
class LoginForm extends Component {
  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="formcenter">
        <form onSubmit={handleSubmit}>
          <h2>Login</h2>
          <Field
            className="inputbox"
            name="username"
            component="input"
            type="text"
            placeholder="Username"
          />
          <Field
            className="inputbox"
            name="password"
            component="input"
            type="password"
            placeholder="Password"
          />

          <button className="submit" type="submit" label="submit">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

LoginForm = reduxForm({
  form: "login"
})(LoginForm);

export default LoginForm;
