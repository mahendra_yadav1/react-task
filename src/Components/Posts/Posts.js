import React, { Component } from "react";
import Spinner from "../Spinner/Spinner";
import { connect } from "react-redux";

class Posts extends Component {
  state = {
    posts: false,
    connPosts: []
  };
  componentDidMount() {
    this.props.connections.map(con => {
      fetch(`http://localhost:3001/${con}`)
        .then(data => data.json())
        .then(data => {
          this.setState(prevState => {
            return {
              connPosts: [...prevState.connPosts, ...data.posts],
              posts: true
            };
          });
        })
        .catch(err => console.log(err));
      return null;
    });
  }

  render() {
    let post = <Spinner />;

    if (this.state.posts) {
      post = this.state.connPosts.map((post, index) => {
        return (
          <div key={index} className="post">
            <h3>{post.title}</h3>
            <p>{post.desc}</p>
          </div>
        );
      });
    }

    return <div className="posts">{this.state.posts ? post : null}</div>;
  }
}

const mapStateToProps = state => {
  return {
    connections: state.data.userData.connections
  };
};
export default connect(mapStateToProps)(Posts);
