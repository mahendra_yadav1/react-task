import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
const myPosts = props => {
  let posts = props.posts.map((post, index) => {
    return (
      <div key={index} className="post">
        <h3>{post.title}</h3>
        <p>{post.desc}</p>
      </div>
    );
  });

  return <div className="posts">{posts}</div>;
};
const mapStateToProps = state => {
  return {
    posts: state.data.userData.posts
  };
};

export default connect(mapStateToProps)(withRouter(myPosts));
