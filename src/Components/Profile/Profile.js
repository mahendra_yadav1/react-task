import React from "react";
import "./profile.css";
import { connect } from "react-redux";

const profile = props => {
  return (
    <div className="centre">
      <img src={props.userData.avatar} className="image" alt="Avatar" />
      <h3>{props.userData.fullname}</h3>
      <h4>{props.userData.status}</h4>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    userData: state.data.userData
  };
};
export default connect(mapStateToProps)(profile);
