import store from "../store";

export const fetch_post = () => {
  return {
    type: "FETCH_USER"
  };
};

export const receive_post = post => {
  return {
    type: "FETCHED_USER",
    data: post
  };
};

export const receive_error = () => {
  return {
    type: "RECEIVE_ERROR"
  };
};

export const thunk_action_creator = username => {
  const user = username.replace(/\s/g, "");

  store.dispatch(fetch_post());
  return function(dispatch) {
    return fetch(`http://localhost:3001/${user}`)
      .then(data => data.json())
      .then(data => {
        if (data.username === undefined) {
          window.alert("user not available");
        } else dispatch(receive_post(data));
      })
      .catch(err => {
        dispatch(receive_error());
      });
  };
};
