import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import asyncReducer from "./reducers/index";
import { reducer as formReducer } from "redux-form";
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const reducers = { form: formReducer, data: asyncReducer };
const reducer = combineReducers(reducers);

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
