import React, { Component } from "react";
import "./App.css";
import LoginForm from "./Components/Auth/Login";
import Profile from "./Components/Profile/Profile";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Posts from "./Components/Posts/Posts";
import MyPosts from "./Components/Posts/MyPosts";
import Connections from "./Components/Connections/Connections";
import AccSettings from "./Components/Settings/AccSettings";
import { connect } from "react-redux";
import { Route, withRouter, Switch } from "react-router-dom";
import { thunk_action_creator } from "./store/actions/fetchAction";
import Settings from "./Components/Settings/settings";
import Spinner from "./Components/Spinner/Spinner";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {},
      connPosts: [],
      loggedIn: false,
      loading: false,
      password: "",
      validate: false
    };
  }

  logoutHandler = () => {
    this.setState({ loggedIn: false });
    this.props.history.replace("/login");
  };
  submitHandler = values => {
    const username = values.username;
    const password = values.password;
    this.setState({ password: password, loading: true });
    this.props.dispatch(thunk_action_creator(username));

    setTimeout(() => {
      const flag = password === this.props.data.userData.password;
      if (flag) {
        this.setState({ loggedIn: true, loading: false });

        this.props.history.push(`/${this.props.data.userData.username}`);
      } else {
        window.alert("Please enter valid credientials!!!");
        this.setState({ loading: false });
      }
    }, 1000);
  };

  dashboard = () => {
    return (
      <div className="App">
        <div id="pageHeader">
          <Header handler={this.logoutHandler} />
        </div>

        <div id="mainPosts">
          <Switch>
            <Route
              path={`/${this.props.data.userData.username}`}
              exact
              component={Posts}
            />
            <Route
              path={`/${this.props.data.userData.username}/myposts`}
              component={MyPosts}
            />
            <Route
              path={`/${this.props.data.userData.username}/edit`}
              component={Settings}
            />
          </Switch>
        </div>
        <div id="mainPro">
          <Profile />
        </div>
        <div id="mainAcc">
          <AccSettings />
        </div>
        <div id="mainConn">
          <Connections />
        </div>

        <div id="pageFooter">
          <Footer />
        </div>
      </div>
    );
  };
  componentDidMount() {
    this.props.history.push("/login");
  }
  render() {
    return (
      <>
        {this.state.loading ? (
          <Spinner />
        ) : (
          <Route
            path="/login"
            render={() => <LoginForm onSubmit={this.submitHandler} />}
          />
        )}

        {this.state.loggedIn ? (
          <Route
            path={`/${this.props.data.userData.username}`}
            render={() => this.dashboard()}
          />
        ) : null}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.data
  };
};
export default connect(mapStateToProps)(withRouter(App));
